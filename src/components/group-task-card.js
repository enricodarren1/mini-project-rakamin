import { useEffect, useState } from 'react';
import InputText from './input-text';
import { Popover } from '@mui/material';
import { createItem, deleteItem, getListItems, updateItem } from '../services/task-service';
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import setting from '../assets/icons/setting.svg';
import done from '../assets/icons/done.svg';
import addIcon from '../assets/icons/add-icon.svg';
import arrowRightIcon from '../assets/icons/arrow-right-icon.svg';
import arrowLeftIcon from '../assets/icons/arrow-left-icon.svg';
import editIcon from '../assets/icons/edit-icon.svg';
import deleteIcon from '../assets/icons/delete-icon.svg';
import dangerIcon from '../assets/icons/danger-icon.svg';
import '../css/group-task-card.css';
import '../css/spacer.css';

function GroupTaskCard(props) {

  const [inputValue, setInputValue] = useState({ taskName: "", progress: "" });
  const { taskName, progress } = inputValue;
  const [listItem, setListItem] = useState([]);
  const [selectedItem, setSelectedItem] = useState({});
  const [anchorEl, setAnchorEl] = useState(null);
  const [randomNumber, setRandomNumber] = useState(null);

  const arrayOfColor = [
    {
      primaryColor: '#F7FEFF',
      secondaryColor: '#01959F'
    },
    {
      primaryColor: '#FFFCF5',
      secondaryColor: '#FEEABC'
    },
    {
      primaryColor: '#FFFAFA',
      secondaryColor: '#F5B1B7'
    },
    {
      primaryColor: '#F8FBF9',
      secondaryColor: '#B8DBCA'
    }
  ]

  const handleClick = (event, data) => {
    setAnchorEl(event.currentTarget);
    setSelectedItem(data)
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);

  const cardStyle = {
    backgroundColor: `${arrayOfColor[Math.floor(randomNumber)].primaryColor}`,
    border: `1px solid ${arrayOfColor[Math.floor(randomNumber)].secondaryColor}`
  }

  const groupTaskStyle = {
    color: props.groupTaskList.secondaryColor
  }

  const createTask = (todoId) => {
    const body = {
      name: taskName,
      progress_percentage: progress
    }
    createItem(todoId, body).then(() => {
      getListItems(props.groupTaskList.id).then(res => {
        setListItem(res.data);
        setInputValue({ taskName: "", progress: "" })
        handleClose();
      });
    });
  }

  const updateSelectedItem = (type = null, todoId, itemId, action) => {
    let tempTodoId = todoId;
    const body = {
      target_todo_id: action !== 'update' ? type === 'right' ? todoId += 1 : todoId -= 1 : tempTodoId
    }
    if (action === 'update') {
      body.name = taskName;
      body.progress_percentage = progress;
    }
    updateItem(tempTodoId, itemId, body).then(() => {
      getListItems(props.groupTaskList.id).then(res => {
        setListItem(res.data);
        handleClose();
      });
    });
  }

  const onMoveTask = (type, todoId, itemId) => {
    updateSelectedItem(type, todoId, itemId, 'move');
  }

  const onEditTask = (todoId, itemId) => {
    updateSelectedItem(null, todoId, itemId, 'update');
  }

  const onDeleteTask = (todoId, itemId) => {
    deleteItem(todoId, itemId).then(() => {
      getListItems(props.groupTaskList.id).then(res => {
        setListItem(res.data);
        handleClose();
      });
    });
  }

  const handleChange = (e) => {
    const { name, value } = e.target;
    setInputValue((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const handleDrop = (droppedItem) => {
    if (!droppedItem.destination) return;
    var updatedList = [...listItem];
    const [reorderedItem] = updatedList.splice(droppedItem.source.index, 1);
    updatedList.splice(droppedItem.destination.index, 0, reorderedItem);
    setListItem(updatedList);
  };

  useEffect(() => {
    getListItems(props.groupTaskList.id).then(res => {
      setListItem(res.data);
    });
    setRandomNumber(Math.random() * arrayOfColor.length);
  }, [props.groupTaskList.id, arrayOfColor.length]);

  return (
    <div style={cardStyle} className='card-container'>

      <div style={cardStyle} className='group-task-label-container mb-8'>
        <span style={groupTaskStyle} className='group-task-label'>{props.groupTaskList.title}</span>
      </div>
      <div className='mb-8'>
        <span className='group-task-deadline'>{props.groupTaskList.description}</span>
      </div>
      <DragDropContext onDragEnd={handleDrop}>
        <Droppable droppableId="list-container">
          {(provided) => (
            <div
              className="list-container"
              {...provided.droppableProps}
              ref={provided.innerRef}>
              {listItem.length === 0 ?
                <div className='empty-task-container mb-8'>
                  <span className='empty-task-label'>No Task</span>
                </div>
                :
                listItem.map((data, i) => {
                  return (
                    <Draggable key={(data.id).toString()} draggableId={(data.id).toString()} index={i}>
                      {(provided) => (
                        <div
                          className="item-container"
                          ref={provided.innerRef}
                          {...provided.dragHandleProps}
                          {...provided.draggableProps}
                        >
                          <div className={`task-container ${i !== listItem.length - 1 ? 'mb-12' : 'mb-8'}`} key={i}>
                            <div>
                              <span className='task-label'>{data.name}</span>
                            </div>
                            <div className='row align-items-center'>
                              <div className='col-8'>
                                <div className="progress" role="progressbar" aria-label="Basic example" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100">
                                  <div className={`progress-bar ${data.progress_percentage !== 100 ? 'custom-progress-bar' : 'custom-progress-bar-done'}`} style={{ width: `${data.progress_percentage}%` }}></div>
                                </div>
                              </div>
                              <div className='col-2'>
                                {
                                  data.progress_percentage !== 100 ? <span className='progress-label'>{data.progress_percentage}%</span> : <img src={done} alt='done' />
                                }
                              </div>
                              <div className='col-2 text-right'>
                                <img src={setting} className='btn-setting' alt='setting' aria-describedby={data.id} variant="contained" onClick={(ev) => handleClick(ev, data)} />
                                <Popover
                                  id={data.id}
                                  open={open}
                                  anchorEl={anchorEl}
                                  onClose={handleClose}
                                  anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'left',
                                  }}
                                >
                                  <div className='popover-container'>
                                    <div className='popover-list mb-12' onClick={() => onMoveTask('right', selectedItem.todo_id, selectedItem.id)}>
                                      <div>
                                        <img src={arrowRightIcon} alt='arrow right icon' />
                                      </div>
                                      <div>
                                        <span className='popover-label'>Move Right</span>
                                      </div>
                                    </div>
                                    <div className='popover-list mb-12' onClick={() => onMoveTask('left', selectedItem.todo_id, selectedItem.id)}>
                                      <div>
                                        <img src={arrowLeftIcon} alt='arrow left icon' />
                                      </div>
                                      <div>
                                        <span className='popover-label'>Move Left</span>
                                      </div>
                                    </div>
                                    <div className='popover-list mb-12'  data-bs-toggle="modal" data-bs-target={`#editTask-${selectedItem.id}`} onClick={handleClose}>
                                      <div>
                                        <img src={editIcon} alt='edit icon' />
                                      </div>
                                      <div>
                                        <span className='popover-label'>Edit</span>
                                      </div>
                                    </div>
                                    <div className='popover-list mb-12' data-bs-toggle="modal" data-bs-target={`#deleteTask-${selectedItem.id}`} onClick={handleClose}>
                                      <div>
                                        <img src={deleteIcon} alt='delete icon' />
                                      </div>
                                      <div>
                                        <span className='popover-label delete-hover'>Delete</span>
                                      </div>
                                    </div>
                                  </div>
                                </Popover>
                              </div>
                            </div>
                          </div>
                          {provided.placeholder}
                        </div>
                      )}

                    </Draggable>
                  )

                })
              }

              <div className='d-flex align-items-center btn-new-task' data-bs-toggle="modal" data-bs-target={`#createTask-${props.groupTaskList.id}`}>
                <div className='pr-7'>
                  <img src={addIcon} alt='add icon' />
                </div>
                <div>
                  <span>New Task</span>
                </div>
              </div>

              {/* START MODAL CREATE TASK */}
              <div className="modal fade" id={`createTask-${props.groupTaskList.id}`} tabIndex="-1" aria-labelledby="createTaskLabel" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered">
                  <div className="modal-content">
                    <div className="modal-header remove-border p-0 mb-24">
                      <h1 className="modal-title fs-5" id="createTaskLabel">Create Task</h1>
                      <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div className="modal-body p-0 mb-24">
                      <div className='mb-16'>
                        <InputText
                          value={taskName}
                          placeholder='Type your Task Name'
                          type="text"
                          label="Task Name"
                          name="taskName"
                          onChange={handleChange} />
                      </div>

                      <div className='container-progress-input'>
                        <InputText
                          value={progress}
                          placeholder='Progress'
                          type="text"
                          label="Progress"
                          name="progress"
                          onChange={handleChange} />
                      </div>
                    </div>
                    <div className="modal-footer remove-border p-0">
                      <button type="button" className="custom-btn-outline mr-10" data-bs-dismiss="modal">Cancel</button>
                      <button type="button" className="custom-btn" data-bs-dismiss="modal" onClick={() => createTask(props.groupTaskList.id)}>Save Task</button>
                    </div>
                  </div>
                </div>
              </div>
              {/* END MODAL CREATE TASK */}

              {/* START MODAL EDIT TASK */}
              <div className="modal fade" id={`editTask-${selectedItem.id}`} tabIndex="-1" aria-labelledby="editTaskLabel" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered">
                  <div className="modal-content">
                    <div className="modal-header remove-border p-0 mb-24">
                      <h1 className="modal-title fs-5" id="editTaskLabel">Edit Task</h1>
                      <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div className="modal-body p-0 mb-24">
                      <div className='mb-16'>
                        <InputText
                          value={taskName}
                          placeholder='Type your Task Name'
                          type="text"
                          label="Task Name"
                          name="taskName"
                          onChange={handleChange} />
                      </div>
                      
                      <div className='container-progress-input'>
                        <InputText
                          value={progress}
                          placeholder='Progress'
                          type="text"
                          label="Progress"
                          name="progress"
                          onChange={handleChange} />
                      </div>
                    </div>
                    <div className="modal-footer remove-border p-0">
                      <button type="button" className="custom-btn-outline mr-10" data-bs-dismiss="modal">Cancel</button>
                      <button type="button" className="custom-btn" data-bs-dismiss="modal" onClick={() => onEditTask(selectedItem.todo_id, selectedItem.id)}>Save Task</button>
                    </div>
                  </div>
                </div>
              </div>
              {/* END MODAL EDIT TASK */}


              {/* START MODAL CONFIRMATION DELETE TASK */}
              <div className="modal fade" id={`deleteTask-${selectedItem.id}`} tabIndex="-1" aria-labelledby="deleteTaskLabel" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered">
                  <div className="modal-content">
                    <div className="modal-header remove-border p-0 mb-24">
                      <img src={dangerIcon} alt='danger icon' style={{ marginRight: '11.07px' }} />
                      <h1 className="modal-title fs-5" id="deleteTaskLabel">Delete Task</h1>
                      <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div className="modal-body p-0 mb-24">
                      <div>
                        <span>Are you sure want to delete this task? your action can't be reverted.</span>
                      </div>
                    </div>
                    <div className="modal-footer remove-border p-0">
                      <button type="button" className="custom-btn-outline mr-10" data-bs-dismiss="modal">Cancel</button>
                      <button type="button" className="custom-btn-danger" data-bs-dismiss="modal" onClick={() => onDeleteTask(selectedItem.todo_id, selectedItem.id)}>Delete</button>
                    </div>
                  </div>
                </div>
              </div>
              {/* END MODAL CONFIRMATION DELETE TASK */}

              {provided.placeholder}
            </div>
          )}

        </Droppable>
      </DragDropContext>
    </div>
  );
}

export default GroupTaskCard;
