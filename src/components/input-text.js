import '../css/input-text.css'

const InputText = ({ value, label, name, placeholder, type, onChange }) => (
  <div className="form-group">
    {label && <label className='label-input' htmlFor="input-field">{label}</label>}
    <input
      type={type}
      value={value}
      name={name}
      className="custom-input-text"
      placeholder={placeholder}
      onChange={onChange}
    />
  </div>
);

export default InputText;