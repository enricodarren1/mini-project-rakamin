import '../css/input-text-area.css'

const InputTextArea = ({ value, label, name, placeholder, type, onChange }) => (
  <div className="form-group">
    {label && <label className='label-input' htmlFor="input-field">{label}</label>}
    <textarea
      type={type}
      value={value}
      name={name}
      className="custom-input-text-area"
      placeholder={placeholder}
      onChange={onChange}
    />
  </div>
);

export default InputTextArea;