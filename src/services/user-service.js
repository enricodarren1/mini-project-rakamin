import axios from 'axios';

axios.interceptors.request.use(
  config => {
    config.headers['Authorization'] = `Bearer ${sessionStorage.getItem('auth_token')}`;
        return config;
    },
    error => {
        return Promise.reject(error);
    }
);

export const signIn = (body) => {
  return axios.post('https://todo-api-18-140-52-65.rakamin.com/auth/login', body);
}

export const signUp = (body) => {
  return axios.post('https://todo-api-18-140-52-65.rakamin.com/signup', body);
}