import axios from 'axios';

axios.interceptors.request.use(
  config => {
    config.headers['Authorization'] = `Bearer ${sessionStorage.getItem('auth_token')}`;
        return config;
    },
    error => {
        return Promise.reject(error);
    }
);

export const getListTodos = () => {
  return axios.get(`https://todo-api-18-140-52-65.rakamin.com/todos`);
}

export const createTodo = (body) => {
  return axios.post(`https://todo-api-18-140-52-65.rakamin.com/todos`, body);
}

export const getListItems = (todoId) => {
  return axios.get(`https://todo-api-18-140-52-65.rakamin.com/todos/${todoId}/items`);
}

export const createItem = (todoId, body) => {
  return axios.post(`https://todo-api-18-140-52-65.rakamin.com/todos/${todoId}/items`, body);
}

export const updateItem = (todoId, itemId, body) => {
  return axios.patch(`https://todo-api-18-140-52-65.rakamin.com/todos/${todoId}/items/${itemId}`, body);
}

export const deleteItem = (todoId, itemId) => {
  return axios.delete(`https://todo-api-18-140-52-65.rakamin.com/todos/${todoId}/items/${itemId}`);
}