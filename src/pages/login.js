import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { signIn } from "../services/user-service";
import InputText from "../components/input-text";
import '../css/login.css';
import '../css/spacer.css';

const Login = () => {
  const [inputValue, setInputValue] = useState({ email: "", password: "" });
  const {email, password} = inputValue;
  const [errMessage, setErrMessage] = useState('');
  const navigate = useNavigate();

  const handleChange = (e) => {
    const { name, value } = e.target;
    setInputValue((prev) => ({
      ...prev,
      [name]: value,
    }));
    setErrMessage('');
  };

  const login = () => {
    const body = {
      // email: 'test@gmail.com',
      // password: 'qweasd'
      email,
      password
    }
    signIn(body).then(res => {
      sessionStorage.setItem('auth_token', res.data.auth_token);
      setInputValue({ email: "", password: "" })
      navigate('/home');
    }, err => {
      setErrMessage(err.response.data.message);
    });
  }

  return (
    <div className="container-login">
      <div>
        <div className="mb-16">
          <InputText
            value={email}
            placeholder='Type your Email'
            type="text"
            label="Email"
            name="email"
            onChange={handleChange}
          />
        </div>
        <div className="mb-16">
          <InputText
            value={password}
            placeholder='*******'
            type="password"
            label="Password"
            name="password"
            onChange={handleChange}
          />
        </div>
        {
          errMessage ? (
            <div className="mb-16">
              <span className="err-label">{errMessage}</span>
            </div>
          ) : null
        }
        <div className="mb-16">
          <button className="custom-btn" onClick={login}>signIn</button>
        </div>
        <div className="mb-8">
          <span>Click Sign Up, if you don't have an account click <span className="link-label" onClick={() => navigate('/register')}>here</span></span>
        </div>
      </div>
    </div>
  );
}

export default Login;
