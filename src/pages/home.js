import { useEffect, useState } from 'react';
import { createTodo, getListTodos } from '../services/task-service';
import { useNavigate } from 'react-router-dom';
import GroupTaskCard from '../components/group-task-card';
import InputText from '../components/input-text';
import InputTextArea from '../components/input-text-area';
import '../css/home.css';

const Home = () => {

  const session = sessionStorage.getItem('auth_token');
  const [inputValue, setInputValue] = useState({ title: "", description: "" });
  const { title, description } = inputValue;
  const [dataGroupTask, setDatadGroupTask] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    if (session) {
      getListTodos().then(res => {
        setDatadGroupTask(res.data);
      })
    } else {
      navigate('/login');
    }
  }, [session, navigate])

  const createGroupTask = () => {
    const body = {
      title,
      description,
      primaryColor: 'red',
      secondaryColor: 'blue'
    }

    createTodo(body).then(() => {
      getListTodos().then(res => {
        setDatadGroupTask(res.data);

        // SET EMPTY
        setInputValue({ title: "", description: "" })
      })
    });
  }

  const handleChange = (e) => {
    const { name, value } = e.target;
    setInputValue((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  return (
    <div>
      <header className='header-container'>
        <div>
          <span className='header-title'>Product Roadmap</span>
        </div>
        <div>
          <button type="button" className="custom-btn" data-bs-toggle="modal" data-bs-target="#creteGroupTask">+ Add New Group</button>
        </div>
      </header>

      <div className='group-task-container'>
        {
          dataGroupTask.map((data, i) => {
            return (
              <div key={i}>
                <GroupTaskCard groupTaskList={data} />
              </div>
            )
          })
        }
      </div>

      {/* MODAL CREATE GROUP TASK */}
      <div className="modal fade" id="creteGroupTask" tabIndex="-1" aria-labelledby="createGroupTaskLabel" aria-hidden="true">
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="modal-header remove-border p-0 mb-24">
              <h1 className="modal-title fs-5" id="createGroupTaskLabel">Create Group Task</h1>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div className="modal-body p-0 mb-24">
              <div className='mb-16'>
                <InputText
                  value={title}
                  placeholder='Type your Title'
                  type="text"
                  label="Title"
                  name="title"
                  onChange={handleChange} />
              </div>

              <div>
                <InputTextArea
                  value={description}
                  placeholder='Type your Description'
                  type="text"
                  label="Description"
                  name="description"
                  onChange={handleChange} />
              </div>
            </div>
            <div className="modal-footer remove-border p-0">
              <button type="button" className="custom-btn-outline mr-10" data-bs-dismiss="modal">Cancel</button>
              <button type="button" className="custom-btn" data-bs-dismiss="modal" onClick={createGroupTask}>Save Task</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Home;
