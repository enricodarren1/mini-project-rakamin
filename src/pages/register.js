import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { signUp } from "../services/user-service";
import InputText from "../components/input-text";
import '../css/login.css';
import '../css/spacer.css';

const Register = () => {
  const [inputValue, setInputValue] = useState({ name: "", email: "", password: "", passwordConfirmation: "" });
  const {name, email, password, passwordConfirmation} = inputValue;
  const [errMessage, setErrMessage] = useState('');
  const navigate = useNavigate();

  const handleChange = (e) => {
    const { name, value } = e.target;
    setInputValue((prev) => ({
      ...prev,
      [name]: value,
    }));
    setErrMessage('');
  };

  const register = () => {
    const body = {
      name,
      email,
      password,
      password_confirmation: passwordConfirmation
    }
    signUp(body).then(res => {
      sessionStorage.setItem('auth_token', res.data.auth_token);
      setInputValue({ name: "", email: "", password: "", password_confirmation: "" })
      navigate('/home');
    }, err => {
      setErrMessage(err.response.data.message);
    });
  }

  return (
    <div className="container-login">
      <div>
        <div className="mb-16">
          <InputText
            value={name}
            placeholder='Type your Name'
            type="text"
            label="Name"
            name="name"
            onChange={handleChange}
          />
        </div>
        <div className="mb-16">
          <InputText
            value={email}
            placeholder='Type your Email'
            type="text"
            label="Email"
            name="email"
            onChange={handleChange}
          />
        </div>
        <div className="mb-16">
          <InputText
            value={password}
            placeholder='*******'
            type="password"
            label="Password"
            name="password"
            onChange={handleChange}
          />
        </div>
        <div className="mb-16">
          <InputText
            value={passwordConfirmation}
            placeholder='*******'
            type="password"
            label="Password Confirmation"
            name="passwordConfirmation"
            onChange={handleChange}
          />
        </div>
        {
          errMessage ? (
            <div className="mb-16">
              <span className="err-label">{errMessage}</span>
            </div>
          ) : null
        }
        <div className="mb-16">
          <button className="custom-btn" onClick={register}>Sign Up</button>
        </div>
      </div>
    </div>
  );
}

export default Register;
